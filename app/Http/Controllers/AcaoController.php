<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Acao;

class AcaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cadastraracao');
    }

    public function cadastrarAcao(Request $request)
    {
        $acao = new Acao;
        $acao->nome = $request->nome;
        $acao->data = $request->data;
        $acao->instituicao = $request->instituicao;
        $acao->local = $request->local;
        $acao->imagem = $request->imagem;
        if($request->boolAcaoMes !== null)
        {
            $acao->boolAcaoMes = true;
        }
        else
        {
            $acao->boolAcaoMes = false;
        }
        
        $acao->save();

        return redirect('acao/cadastrar')->with('status', 'Ação cadastrada com sucesso.');
    }

    public function visualizarAcao()
    {
        $acoes = DB::table('Acao')->get();
        return view('visualizaracao', ['acoes' => $acoes]);
        
    }

    public function excluirAcao(Request $request)
    {
        DB::table('Acao')->where('idAcao', '=', $request->idAcao)->delete();
        return redirect('acao/visualizar');
    }

    public function editarAcao(Request $request)
    {
        if($request->boolAcaoMes !== null)
        {
            $boolAcaoMes = true;
        }
        else
        {
            $boolAcaoMes = false;
        }

        DB::table('Acao')
            ->where('idAcao', $request->idAcao)
            ->update(['nome' => $request->nome, 'data' => $request->data, 'instituicao' => $request->instituicao,
            'local' => $request->local, 'imagem' => $request->imagem, 'boolAcaoMes' => $boolAcaoMes]);

        return redirect('acao/visualizar');
    }
}
