<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ConfiguracaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function mudarSenha(Request $request)
    {
       //VALIDAÇÃO
        $validator = Validator::make($request->all(), [
            'SenhaAtual' => 'required',
            'NovaSenha' => 'required|min:8',
            'RepitaANovaSenha' => 'required|same:NovaSenha',
        ]);

        if ($validator->fails()) {
            return redirect('/mudarsenha')
                        ->withErrors($validator)
                        ->withInput();
        }
        //SENHA ATUAL NAO CORRESPONDE
        if (!Hash::check($request->SenhaAtual, Auth::User()->password)) {
           
            $validator = [
                "senhaigual" => "Senha atual digitada não corresponde com as dos nossos registros.",
            ];
            return redirect('/mudarsenha')
                        ->withErrors($validator)
                        ->withInput();
        }
        //NOVA SENHA IGUAL A ATUAL
        if (Hash::check($request->NovaSenha, Auth::User()->password)) {
           
            $validator = [
                "senhaigual" => "A nova senha não pode ser igual à atual.",
            ];
            return redirect('/mudarsenha')
                        ->withErrors($validator)
                        ->withInput();
        }
        else
        {
            DB::table('Usuario')
            ->where('idUsuario', Auth::User()->idUsuario)
            ->update(['password' => Hash::make($request->NovaSenha)]);

            return redirect('/mudarsenha')->with('sucesso', 'Senha alterada com sucesso.');
        }
        
    }
}