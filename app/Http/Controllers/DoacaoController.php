<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Doacao;

class DoacaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $acoes = DB::table('Acao')->get();
        $categorias = DB::table('Categoria')->get();
        return view('cadastrardoacao', ['acoes' => $acoes,
                                        'categorias' => $categorias,
        ]);
    }

    public function cadastrarDoacao(Request $request)
    {
        $doacao = new Doacao;
        $doacao->nome = $request->nome;
        $doacao->descricao = $request->descricao;
        $doacao->idCategoria = $request->categoria;
        $doacao->idAcao = $request->acao;
        $doacao->idUsuario = Auth::user()->idUsuario;
        
        $doacao->save();

        return redirect('doacao/cadastrar')->with('status', 'Doação cadastrada com sucesso.');
    }
    public function visualizarDoacao()
    {
        $acoes = DB::table('Acao')->get();
        $categorias = DB::table('Categoria')->get();
        $doacoes = DB::table('Doacao')
            ->join('Categoria', 'Doacao.idCategoria', '=', 'Categoria.idCategoria')
            ->join('Acao', 'Doacao.idAcao', '=', 'Acao.idAcao')
            ->join('Usuario', 'Doacao.idUsuario', '=', 'Usuario.idUsuario')
            ->select('Doacao.nome as NomeDoacao', 'Doacao.descricao as DescricaoDoacao',
                    'Categoria.nome as NomeCategoria',
                    'Categoria.idCategoria',
                    'Acao.nome as NomeAcao',
                    'Acao.idAcao',
                    'Doacao.idDoacao', 
                    'Usuario.nome as NomeUsuario')
            ->get();

        return view('visualizardoacao', ['doacoes' => $doacoes,
                                        'acoes' => $acoes,
                                        'categorias' => $categorias,]);
    }

    public function editarDoacao(Request $request)
    {
        DB::table('Doacao')
            ->where('idDoacao', $request->idDoacao)
            ->update(['idAcao' => $request->acao, 
            'idCategoria' => $request->categoria, 
            'nome' => $request->nome,
            'descricao' => $request->descricao]);
        
        return redirect('doacao/visualizar');
    }

    public function excluirDoacao(Request $request)
    {
        DB::table('Doacao')->where('idDoacao', '=', $request->idDoacao)->delete();
        return redirect('doacao/visualizar');
    }
}
