<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doacao extends Model
{
    protected $fillable = [
        'nome', 'descricao'
    ];

    protected $table = 'Doacao';

    public $timestamps = false;
}
