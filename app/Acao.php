<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acao extends Model
{
    
    protected $fillable = [
        'nome', 'data', 'instituicao', 'local', 'imagem', 'boolAcaoMes'
    ];

    protected $table = 'Acao';

    public $timestamps = false;
}
