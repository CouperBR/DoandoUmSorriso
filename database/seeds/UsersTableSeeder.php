<?php

use Illuminate\Database\Seeder;
use App\Usuario;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'login' => 'Admin',
            'email' => 'douglasmsp@hotmail.com',
            'password' => Hash::make('123456'),
            'nome'    => 'Admin',
            'boolAdmin' => true
        ]);
    }
}
