<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
//DOACAO
Route::get('/doacao/cadastrar', 'DoacaoController@index')->name('doacao');
Route::post('/doacao/cadastrar', 'DoacaoController@cadastrarDoacao')->name('doacao');
Route::get('/doacao/visualizar', 'DoacaoController@visualizarDoacao')->name('doacao');
Route::post('/doacao/excluir', 'DoacaoController@excluirDoacao')->name('doacao');
Route::post('/doacao/editar', 'DoacaoController@editarDoacao')->name('doacao');

//ACAO
Route::get('/acao/cadastrar', 'AcaoController@index')->name('acaoget');
Route::post('/acao/cadastrar', 'AcaoController@cadastrarAcao')->name('cadastrarAcao');
Route::get('/acao/visualizar', 'AcaoController@visualizarAcao')->name('visualizarAcao');
Route::post('/acao/excluir', 'AcaoController@excluirAcao')->name('excluirAcao');
Route::post('/acao/editar', 'AcaoController@editarAcao')->name('editarAcao');

//MUDAR SENHA
Route::post('/mudarsenha', 'ConfiguracaoController@mudarSenha')->name('mudarSenha');
Route::get('/mudarsenha', function () {
    return view('mudarsenha');
})->middleware('auth');

//INTEGRANTE
Route::get('/integrante/cadastrar', function () {
    return view('formulario');
})->middleware('guest');