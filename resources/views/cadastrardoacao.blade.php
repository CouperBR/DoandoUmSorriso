@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Cadastre novas doações :)</h1>
@stop

@section('content')
<div class="col-lg-6">
    <form action="{{ url('doacao/cadastrar') }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input required="" type="text" name="nome" class="form-control"
                           placeholder="Nome da Doação">
                    <span class="glyphicon glyphicon-font form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                           <textarea required="" placeholder="Descrição" class="form-control"  name="descricao" cols="40" rows="5"></textarea>
                    <span class="glyphicon glyphicon-align-right form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" name="categoria" required="">
                        <option value="" disabled selected>Categoria</option>
                        @if(isset($categorias))
                        @foreach($categorias as $categoria)
                        <option value="{{ $categoria->idCategoria }}">{{ $categoria->nome }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group has-feedback">
                    <select class="form-control" name="acao" required="">
                        <option value="" disabled selected>Ação</option>
                        @if(isset($acoes))
                        @foreach($acoes as $acao)
                        <option value="{{ $acao->idAcao }}">{{ $acao->nome }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit"
                                class="btn btn-primary btn-block btn-flat">Cadastrar</button>
                    </div>
                    <!-- /.col -->
                </div>
                @if (session('status'))
                <div class="row">
                    <div class="alert alert-success col-xs-12">
                        <ul>
                            <li>{{ session('status') }}</li>
                        </ul>
                    </div> 
                </div>
                @endif
            </form>
</div>
@stop