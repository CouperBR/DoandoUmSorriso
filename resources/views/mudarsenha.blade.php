@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Mude sua senha !</h1>
@stop

@section('content')
<div class="col-lg-6">
    <form action="{{ url('mudarsenha') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group has-feedback">
        <label for="formGroupExampleInput">Senha Atual</label>
        <input type="password" name="SenhaAtual" class="form-control" placeholder="Senha Atual">
    </div>
    <div class="form-group has-feedback">
        <label for="formGroupExampleInput">Nova Senha</label>
        <input type="password" name="NovaSenha" class="form-control" placeholder="Nova Senha">
    </div>
    <div class="form-group has-feedback">
        <label for="formGroupExampleInput">Digite Novamente</label>
        <input type="password" name="RepitaANovaSenha" class="form-control" placeholder="Digite novamente">
    </div>
    <div class="form-group has-feedback">
        <button type="submit" class="btn btn-primary">Confirmar</button>
    </div>
    </form>
    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif
    @if (isset($sucesso))
            <div class="alert alert-success">
                <ul>
                    @foreach ($sucesso as $sus)
                        <li>{{ $sus }}</li>
                    @endforeach
                </ul>
            </div>
    @endif
</div>
@stop