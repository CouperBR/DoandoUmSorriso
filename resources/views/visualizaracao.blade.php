@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Visualize aqui, as ações já cadastradas :)</h1>
@stop

@section('content')
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nome</th>
                  <th>Data</th>
                  <th>Instituição</th>
                  <th>Local</th>
                </tr>
                </thead>
                <tbody>
              @foreach ($acoes as $acao)
                <tr>
                  <td>{{ $acao->nome }}</td> 
                  <td>{{ $acao->data }}</td>
                  <td>{{$acao->instituicao}}</td>
                  <td>{{$acao->local}}</td>
                  <td>
                  
                  <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditar{{ $acao->idAcao }}"><span class="glyphicon glyphicon-pencil"></button>
                  </td>
                  <td>
                  <button class="btn btn-danger btn-sm" id="{{ $acao->idAcao }}" data-toggle="modal" data-target="#modalExcluir{{ $acao->idAcao }}"><span class="glyphicon glyphicon-trash"></button>
                  </td>
                  
                </tr>
                
                <!-- COMEÇO MODAL EXCLUIR -->
                <div class="modal fade" id="modalExcluir{{ $acao->idAcao }}" tabindex="-1" role="dialog" aria-labelledby="modalEditar" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Excluir ação</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <span> Deseja realmente excluir?</span>
                          <form action="{{ url('acao/excluir') }}" method="post">
                              {{ csrf_field() }}
                              <input name="idAcao" type="hidden" value="{{ $acao->idAcao }}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-danger">Excluir</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- FIM MODAL EXCLUIR -->
                  <!-- COMEÇO MODAL EDITAR -->
                  <div class="modal fade" id="modalEditar{{$acao->idAcao}}" tabindex="-1" role="dialog" aria-labelledby="modalEditar" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Editar ação</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('acao/editar') }}" method="post">
                                {{ csrf_field() }}
                                <input name="idAcao" type="hidden" value="{{ $acao->idAcao }}">
                                <div class="form-group has-feedback">
                                    <input required="" type="text" name="nome" class="form-control"
                                placeholder="Nome da Ação" value="{{ $acao->nome }}">
                                    <span class="glyphicon glyphicon-font form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                
                                    <input required="" type="datetime" name="data" class="form-control"
                                           placeholder="Data" value="{{ $acao->data }}">  <span class="label label-primary">DATA E HORÁRIO</span>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input required="" type="text" name="instituicao" class="form-control"
                                           placeholder="Instituição" value="{{ $acao->instituicao }}">
                                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                                </div>
                                 <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input required="" type="text" name="local" class="form-control"
                                           placeholder="Local" value="{{ $acao->local }}">
                                    <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                                </div>
                                 <div class="form-group has-feedback">
                                 <span class="label label-primary">IMAGEM</span>
                                    <input type="file" accept="image/*" class="form-control-file" id="imagem" aria-describedby="fileHelp">
                                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                                </div>
                                
                                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="checkbox" name="boolAcaoMes" class="form-check-input"> <span>Ação do Mês</span>
                                    <span class="glyphicon glyphicon-question-sign form-control-feedback"></span>
                                </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  <!-- FIM MODAL EDITAR -->
                <tfoot>
                <tr>
                  <th>Nome</th>
                  <th>Data</th>
                  <th>Instituição</th>
                  <th>Local</th>
                </tr>
                </tfoot>
              </table>
            </div>
@stop