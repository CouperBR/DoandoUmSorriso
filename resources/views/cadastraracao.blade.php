@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Cadastre novas ações <span class="glyphicon glyphicon-thumbs-up"></span></h1>
@stop

@section('content')
<div class="col-lg-6">
    <form action="{{ url('acao/cadastrar') }}" method="post">
    {{ csrf_field() }}
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input required="" type="text" name="nome" class="form-control"
                           placeholder="Nome da Ação">
                    <span class="glyphicon glyphicon-font form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                
                    <input required="" type="datetime-local" name="data" class="form-control"
                           placeholder="Data"> <span class="label label-primary">DATA E HORÁRIO</span>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input required="" type="text" name="instituicao" class="form-control"
                           placeholder="Instituição">
                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                </div>
                 <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input required="" type="text" name="local" class="form-control"
                           placeholder="Local">
                    <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                </div>
                 <div class="form-group has-feedback">
                 <span class="label label-primary">IMAGEM</span>
                    <input type="file" accept="image/*" class="form-control-file" id="imagem" aria-describedby="fileHelp">
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>
                
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="checkbox" name="boolAcaoMes" class="form-check-input"> <span>Ação do Mês</span>
                    <span class="glyphicon glyphicon-question-sign form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar</button>
                    </div>
                </div>
                @if (session('status'))
                <div class="row">
                    <div class="alert alert-success col-xs-12">
                        <ul>
                            <li>{{ session('status') }}</li>
                        </ul>
                    </div> 
                </div>
                @endif     
            </form>
</div>
@stop