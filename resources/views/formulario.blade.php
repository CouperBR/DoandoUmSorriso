<!DOCTYPE html>
<html lang="pt-BR" >
    


<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Doando um Sorriso | Formulário</title>
  
   <!--LINKS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="images/android-icon-192x192.png">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
</head>

<body>
  <!-- multistep form -->
<form id="msform" method="post" action="{{ url('integrante/cadastrar') }}">
  <!-- progressbar -->
  <ul id="progressbar">
    <li class="active">Dados pessoais</li>
    <li>Perguntas</li>
    <li>Apresentação</li>
  </ul>
  <!-- fieldsets -->
  <fieldset>
      <img src="{{ asset('images/logo.jpeg') }}">
    <h2 class="fs-title">Informações Básicas</h2>
    <h3 class="fs-subtitle">Primeiro passo</h3>
    <input required="" type="text" id="nome" name="nome"  placeholder="Nome Completo" />
    <input type="text" id="email" name="email" placeholder="Email" />
    <input type="date" id="data" name="date" placeholder="Data de Nascimento" />
    <select require="" class="form-control form-control-lg">
        <option aria-placeholder="Selecione">Idade</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
        <option>32</option>
        <option>33</option>
        <option>34</option>
        <option>35</option>
        <option>36</option>
        <option>37</option>
        <option>38</option>
        <option>39</option>
        <option>40</option>
        <option>41</option>
        <option>42</option>
        <option>43</option>
        <option>44</option>
        <option>45</option>
        <option>46</option>
        <option>47</option>
        <option>48</option>
        <option>49</option>
        <option>50+</option>

      </select>
    <input type="text" name="cidade" placeholder="Cidade" icon="map-o" />
    <input type="text" name="bairro" placeholder="Bairro" />
    <input type="text" name="whatsapp" id ="txtTelefone" onkeypress="mascaraTelefone()" maxlength="14" placeholder="Whatsapp"/>
    <input type="text" name="instagram" placeholder="Instagram" />
    <input type="button" name="next" class="next action-button" value="Proximo" />
  </fieldset>
  <fieldset>
      <img src="{{ asset('images/logo.jpeg') }}">
    <h2 class="fs-title">Responda as perguntas!</h2>
    <h3 class="fs-subtitle">Segundo Passo</h3>
    <textarea name="caracteristicas" rows="5" placeholder="Quais características considera indispensável para ser um voluntário?"></textarea>
    <textarea name="disponibilidade" rows="5" placeholder="Qual a sua disponibilidade de horários? Favor especificar (dias de semana e finais de semana)."></textarea>
    <textarea name="AtividadesSociais" rows="5" placeholder="Você já fez realizou trabalho voluntário ou algum tipo de ação social?(Se sim conte para a gente)."></textarea>
    <input type="button" name="previous" class="previous action-button" value="Voltar" />
    <input type="button" name="next" class="next action-button" value="Próximo" />
  </fieldset>
  <fieldset>
      <img src="{{ asset('images/logo.jpeg') }}">
    <h2 class="fs-title">Faça uma apresentação sobre você.</h2>
    <h3 class="fs-subtitle">Terceiro e último passo.</h3>
    <textarea name="textarea" rows="15" placeholder="Ex: (profissão, o que estuda, o que gosta de fazer no tempo vago, experiências com voluntariado)."></textarea>
    <input type="button" name="previous" class="previous action-button" value="Voltar" />
    <input type="submit" name="submit" class="submit action-button" value="Enviar" />
  </fieldset>
</form>


 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
 <script  src="{{ asset('js/index.js') }}"></script>
  

  <script>
      function mascaraTelefone(){
        var telefone= document.getElementById('txtTelefone').value;
          if(telefone.length==1){
            document.getElementById('txtTelefone').value ='(' + telefone;
            }
            else if (telefone.length==3){
              document.getElementById('txtTelefone').value = telefone +')';
             }
            else if (telefone.length==9){
              document.getElementById('txtTelefone').value = telefone +'-';
            }
      }
  </script>
  
</body>
</html>
