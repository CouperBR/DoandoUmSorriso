@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Visualize aqui, as ações já cadastradas :)</h1>
@stop

@section('content')
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th>Categoria</th>
                  <th>Ação</th>
                  <th>Quem adicionou?</th>
                </tr>
                </thead>
                <tbody>
              @foreach ($doacoes as $doacao)
                <tr>
                  <td>{{ $doacao->NomeDoacao }}</td> 
                  <td>{{ $doacao->DescricaoDoacao }}</td>
                  <td>{{$doacao->NomeCategoria}}</td>
                  <td>{{$doacao->NomeAcao}}</td>
                  <td>{{$doacao->NomeUsuario}}</td>
                  <td>
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditar{{ $doacao->idDoacao }}"><span class="glyphicon glyphicon-pencil"></button>
                  </td>
                  <td>
                  <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalExcluir{{$doacao->idDoacao}}"><span class="glyphicon glyphicon-trash"></button>
                  </td>
                </tr>

                <!-- COMEÇO MODAL EXCLUIR -->
                <div class="modal fade" id="modalExcluir{{$doacao->idDoacao}}" tabindex="-1" role="dialog" aria-labelledby="modalEditar" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Excluir ação</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <span> Deseja realmente excluir?</span>
                          <form action="{{ url('doacao/excluir') }}" method="post">
                              {{ csrf_field() }}
                              <input name="idDoacao" type="hidden" value="{{ $doacao->idDoacao }}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-danger">Excluir</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- FIM MODAL EXCLUIR -->

                  <!-- COMEÇO MODAL EDITAR -->
                  <div class="modal fade" id="modalEditar{{$doacao->idDoacao}}" tabindex="-1" role="dialog" aria-labelledby="modalEditar" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Editar ação</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('doacao/editar') }}" method="post">
                            {!! csrf_field() !!}
                            <input name="idDoacao" type="hidden" value="{{ $doacao->idDoacao }}">
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input required="" type="text" name="nome" class="form-control"
                                      placeholder="Nome da Doação" value="{{$doacao->NomeDoacao}}">
                                <span class="glyphicon glyphicon-font form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                      <textarea required="" placeholder="Descrição" class="form-control"  name="descricao" cols="40" rows="5">{{$doacao->DescricaoDoacao}}</textarea>
                                <span class="glyphicon glyphicon-align-right form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <select class="form-control" name="categoria" required="">
                                    <option value="" disabled selected>Categoria</option>
                                    @if(isset($categorias))
                                      @foreach($categorias as $categoria)
                                        @if($doacao->idCategoria == $categoria->idCategoria)
                                        <option selected="" value="{{ $categoria->idCategoria }}">{{ $categoria->nome }}</option>
                                        @else
                                        <option value="{{ $categoria->idCategoria }}">{{ $categoria->nome }}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group has-feedback">
                                <select class="form-control" name="acao" required="">
                                    <option value="" disabled selected>Ação</option>
                                    @if(isset($acoes))
                                      @foreach($acoes as $acao)
                                        @if($doacao->idAcao == $acao->idAcao)
                                        <option selected="" value="{{ $acao->idAcao }}">{{ $acao->nome }}</option>
                                        @else
                                        <option value="{{ $acao->idAcao }}">{{ $acao->nome }}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- FIM MODAL EDITAR -->
                @endforeach
                <tfoot>
                <tr>
                  <th>Nome</th>
                  <th>Data</th>
                  <th>Instituição</th>
                  <th>Local</th>
                </tr>
                </tfoot>
              </table>
            </div>
@stop